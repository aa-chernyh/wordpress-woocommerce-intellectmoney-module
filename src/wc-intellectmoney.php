<?php
/*
  Plugin Name: Платежный модуль IntellectMoney
  Plugin URI:
  Description: IntellectMoney — это универсальное решение по приему платежей в интернете. Мы работаем на рынке онлайн-платежей более 10 лет. За это время нас выбрали более 8 000 компаний. Среди них как российские, так и иностранные компании.
  Version: 3.1.0
  Author: IntellectMoney
  Author URI: https://intellectmoney.ru
 */
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
if (!defined('ABSPATH'))
    exit;

include 'IntellectMoneyHelper/MerchantReceiptHelper.php';

add_action('plugins_loaded', 'woocommerce_intellectmoney', 0);

function woocommerce_intellectmoney() {
    if (!class_exists('WC_Payment_Gateway'))
        return;
    if (class_exists('WC_IntellectMoney'))
        return;

    class WC_IntellectMoney extends WC_Payment_Gateway {

        var $outsumcurrency = '';
        var $lang;

        public function __construct() {
            $woocommerce_currency = get_option('woocommerce_currency');
            if (in_array($woocommerce_currency, array('RUB', 'TST'))) {
                $this->currency = $woocommerce_currency;
            }

            $plugin_dir = plugin_dir_url(__FILE__);

            global $woocommerce;

            $this->id = 'intellectmoney';
            $this->icon = apply_filters('woocommerce_intellectmoney_icon', '' . $plugin_dir . 'logo.png');
            $this->has_fields = false;

            $this->init_form_fields();
            $this->init_settings();
            
            $moduleDescription = $this->get_option('im_description');
            if(empty($moduleDescription)) {
                $moduleDescription = 'Оплата с помощью пластиковых карт Visa/Mastercard, СбербанкОнлайн, Яндекс.Деньги, Webmoney, Деньги@Mail.ru, терминалы оплаты, банковский перевод, почта России и т.д.';
                $this->update_option('im_description', $moduleDescription);
            } 
            $this->method_description = __($this->get_option('im_description'), 'woocommerce');
            // Define user set variables
            $this->title = $this->get_option('im_name');
            $this->description = $this->get_option('im_description');
            $this->im_eshopId = $this->get_option('im_eshopId');
            $this->im_secretKey = $this->get_option('im_secretKey');
            $this->im_test = $this->get_option('im_test');
            $this->im_hold = $this->get_option('im_hold');
            $this->im_expireDate = $this->get_option('im_expireDate');
            $this->im_cachbox = $this->get_option('im_cachbox');
            $this->im_inn = $this->get_option('im_inn');
            $this->im_productnds = $this->get_option('im_productnds');
            $this->im_deliverynds = $this->get_option('im_deliverynds');
            $this->im_preference = $this->get_option('im_preference');
            $this->im_successUrl = $this->get_option('im_successUrl');
            $this->im_paymentStatus3 = $this->get_option('im_paymentStatus3');
            $this->im_paymentStatus5 = $this->get_option('im_paymentStatus5');

            // Actions
            add_action('woocommerce_receipt_' . $this->id, array($this, 'receipt_page'));

            // Save options
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

            // Payment listener/API hook
            add_action('woocommerce_api_wc_' . $this->id, array($this, 'check_ipn_response'));

            if (!$this->is_valid_for_use()) {
                $this->enabled = false;
            }
        }

        function is_valid_for_use() {
            if (!in_array(get_option('woocommerce_currency'), array('RUB', 'TST'))) {
                return false;
            }
            return true;
        }

        public function admin_options() {
            ?>
            <h3><?php _e('Платежный модуль IntellectMoney', 'woocommerce'); ?></h3>
            <p><?php _e('Настройка модуля оплаты IntellectMoney', 'woocommerce'); ?></p>

            <?php if ($this->is_valid_for_use()) : ?>
                <table class="form-table">

                    <?php
                    $this->generate_settings_html();
                    ?>
                </table>
                <script>
                    var el = document.getElementById('woocommerce_intellectmoney_im_resultUrl').setAttribute('disabled', 'disabled');
                </script>
            <?php else : ?>
                <div class="inline error"><p><strong><?php _e('Способ оплаты отключен', 'woocommerce'); ?></strong>: <?php _e('Модуль оплаты не поддерживает валюты Вашего магазина.', 'woocommerce'); ?></p></div>
            <?php
            endif;
        }

        function init_form_fields() {
            $statuses = wc_get_order_statuses();
            $nds = array(
                '1' => 'НДС 20%',
                '2' => 'НДС 10%',
                '3' => 'НДС расч. 20/120',
                '4' => 'НДС расч. 10/110',
                '5' => 'НДС 0%',
                '6' => 'НДС не облагается',
            );

            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Включить/Выключить', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Включен', 'woocommerce'),
                    'default' => 'yes'
                ),
                'im_name' => array(
                    'title' => __('Название', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Название способа оплаты, которое будет отображаться при выборе', 'woocommerce'),
                    'default' => 'Оплата через IntellectMoney',
                    'placeholder' => 'Оплата через IntellectMoney',
                    'css' => 'width: 500px;'
                ),
                'im_description' => array(
                    'title' => __('Description', 'woocommerce'),
                    'type' => 'textarea',
                    'description' => __('Описание способа оплаты, которое будет отображаться при выборе', 'woocommerce'),
                    'css' => 'width: 500px;'
                ),
                'im_eshopId' => array(
                    'title' => __('Номер магазина в системе IntellectMoney', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Номер магазина нужно скопировать из личного кабинета IntellectMoney', 'woocommerce'),
                    'placeholder' => '459999',
                ),
                'im_secretKey' => array(
                    'title' => __('Секретный ключ в системе IntellectMoney', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Укажите секретный ключ, такой же, который вы указали в личном кабинете IntellectMoney', 'woocommerce'),
                    'placeholder' => 'mySecretKey',
                    'css' => 'width: 500px;'
                ),
                'im_resultUrl' => array(
                    'title' => __('Result URL', 'woocommerce'),
                    'type' => 'label',
                    'description' => __('Скопируйте данный адрес в личный кабинет IntellectMoney в настройки магазина', 'woocommerce'),
                    'default' => get_site_url() . '/?wc-api=wc_intellectmoney&return=result_url',
                    'placeholder' => get_site_url() . '/?wc-api=wc_intellectmoney&return=result_url',
                    'css' => 'width: 500px;',
                ),
                'im_test' => array(
                    'title' => __('Тестовый режим', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Включен', 'woocommerce'),
                    'description' => __('Включите данный режим при тестировании', 'woocommerce'),
                    'default' => 'no'
                ),
                'im_hold' => array(
                    'title' => __('Режим холдирования', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Включен', 'woocommerce'),
                    'description' => __('Включите данный режим, если хотите холдировать денежные средства'),
                    'default' => 'no'
                ),
                'im_expireDate' => array(
                    'title' => __('Время холдирования в часах', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Данный параметр относится к режиму холдирования. Укажите количество часов, по истечению которых денежные средства автоматически зачислятся на ваш счет, либо будут возвращены клиенту. Максимум 119 часов. По умолчанию 119 часов.', 'woocommerce'),
                    'default' => '119',
                    'css' => 'width: 50px;'
                ),
                'im_cachbox' => array(
                    'title' => __('Онлан-касса', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Включен', 'woocommerce'),
                    'description' => __('Включить передачу параметров для онлайн-кассы', 'woocommerce'),
                    'default' => 'no'
                ),
                'im_inn' => array(
                    'title' => __('ИНН', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Укажите ИНН, такой же, который вы указали в личном кабинете IntellectMoney', 'woocommerce'),
                    'placeholder' => 'ИНН',
                    'css' => 'width: 500px;'
                ),
                'im_productnds' => array(
                    'title' => __('Ставка НДС для товара', 'woocommerce'),
                    'type' => 'select',
                    'description' => __('Выберите ставку НДС для товара', 'woocommerce'),
                    'options' => $nds,
                    'css' => 'width: 280px;'
                ),
                'im_deliverynds' => array(
                    'title' => __('Ставка НДС для доставки', 'woocommerce'),
                    'type' => 'select',
                    'description' => __('Выберите ставку НДС для доставки', 'woocommerce'),
                    'options' => $nds,
                    'css' => 'width: 280px;'
                ),
                'im_paymentStatus3' => array(
                    'title' => __('Статус заказа при создании СКО', 'woocommerce'),
                    'type' => 'select',
                    'description' => __('Выберите статус для созданного заказа', 'woocommerce'),
                    'options' => $statuses,
                    'css' => 'width: 180px;'
                ),
                'im_paymentStatus5' => array(
                    'title' => __('Статус заказа при полной оплате СКО', 'woocommerce'),
                    'type' => 'select',
                    'description' => __('Выберите статус для оплаченного заказа', 'woocommerce'),
                    'options' => $statuses,
                    'css' => 'width: 180px;'
                ),
                'im_preference' => array(
                    'title' => __('Доступные способы оплаты', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('По умолчанию доступны все способы оплаты. Оставьте данное поле пустым, если не знаете что указать', 'woocommerce'),
                    'default' => '',
                    'placeholder' => 'bankcard, sberbank, yandex, webmoney, terminals, alfaclick',
                    'css' => 'width: 500px;'
                ),
                'im_successUrl' => array(
                    'title' => __('Адрес при успешной оплате', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Адрес, на который будет перенаправлен клиент после успешной оплаты. Можно оставить по-умолчанию', 'woocommerce'),
                    'default' => get_site_url() . '/?wc-api=wc_intellectmoney&return=success_url',
                    'placeholder' => get_site_url() . '/?wc-api=wc_intellectmoney&return=success_url',
                    'css' => 'width: 500px;'
                ),
            );
        }

        function payment_fields() {
            if ($this->description) {
                echo wpautop(wptexturize($this->description));
            }
        }

        public function generate_form($order_id) {
            global $woocommerce;

            $order = new WC_Order($order_id);

            $recipientAmount = number_format($order->order_total, 2, '.', '');

            $control_hash_str = implode('::', array(
                $this->im_eshopId, $order_id, __('Оплата заказа №', 'woocommerce') . $order_id,
                $recipientAmount, $this->im_test == 'yes' ? 'TST' : $this->currency, $this->im_secretKey
            ));
            $control_hash = md5($control_hash_str);
            $inn = $this->im_inn;
            $email = $order->billing_email;
            $discount_amount = 0;
            $group = 'Main';
            $merchant_helper = new PaySystem\MerchantReceiptHelper($recipientAmount, $inn, $email, $group, $discount_amount, $recipientAmount);
            if (version_compare($woocommerce->version, "3.0", ">=")) {
                $items = $order->get_items();
                $data = $order->get_data();
                $shipping = $data['shipping_lines'];
                $hasShipping = (bool) count($shipping);
                foreach ($items as $item) {
                    $amount = $item->get_total() / $item->get_quantity();
                    $amount = round($amount, 2);
                    $quantity = $item->get_quantity();
                    $tax = $this->im_productnds;
                    $merchant_helper->addItem($amount, $quantity, $item['name'], $tax);
                }
                if ($hasShipping) {
                    $shippingData = array_shift($shipping);
                    $amount = $shippingData['total'];
                    $amount = round($amount, 2);
                    $tax = $this->im_deliverynds;
                    $merchant_helper->addItem($amount, 1.000, 'Доставка', $tax);
                }
            } else {

                $items = $order->get_items();
                $shipping = $order->get_items('shipping');
                $hasShipping = (bool) count($shipping);
                $orderTotal = number_format($order->order_total, 2, '.', '');
                foreach ($items as $itemId => $item) {
                    $quantity = $order->get_item_meta($itemId, '_qty', true);
                    $itemTotal = $order->get_item_meta($itemId, '_line_total', true);
                    $amount = $itemTotal / $quantity;
                    $amount = round($amount, 2);
                    $tax = $this->im_productnds;
                    $merchant_helper->addItem($amount, $quantity, $item['name'], $tax);
                }
                if ($hasShipping) {
                    $itemId = key($shipping);
                    $amount = $order->get_total_shipping();
                    $amount = round($amount, 2);
                    $tax = $this->im_deliverynds;
                    $merchant_helper->addItem($amount, 1.000, 'Доставка', $tax);
                }
            }

            $fields = array(
                'eshopId' => $this->im_eshopId,
                'orderId' => $order_id,
                'serviceName' => __('Оплата заказа №', 'woocommerce') . $order_id,
                'recipientAmount' => $recipientAmount,
                'recipientCurrency' => $this->im_test == 'yes' ? 'TST' : $this->currency,
                'userName' => $order->billing_first_name . ' ' . $order->billing_last_name,
                'email' => $email,
                'preference' => $this->im_preference,
                'successUrl' => $this->im_successUrl . '&orderId=' . $order->id,
                'hash' => $control_hash
            );

            if ($this->im_hold == 'yes') {
                $fields['holdMode'] = '1';
                $hour = $this->im_expireDate;
                if (intval($hour) && $hour > 0 && $hour < 119) {
                    $fields['expireDate'] = date('Y-m-d H:i:s', strtotime('+' . $hour . ' hours'));
                } else {
                    $fields['expireDate'] = date('Y-m-d H:i:s', strtotime('+119 hours'));
                }
            }
            if ($this->im_cachbox == 'yes') {
                $fields['merchantReceipt'] = $merchant_helper->generateMerchantReceipt();
            }

            $postData = array();
            foreach ($fields as $key => $value) {
                $postData[] = '<input type="hidden" name="' . esc_attr($key) . '" value="' . esc_attr($value) . '" />';
            }

            return
                    '<form id="im_pay" action="https://merchant.intellectmoney.ru/ru/" method="POST">' . "\n" .
                    implode("\n", $postData) . "\n" .
                    '</form>' .
                    '<script>
                        if (window.jQuery) {
                            jQuery(document).ready(function() { jQuery("#im_pay").submit(); });
                        }
                        else{
                            google.load("jquery", "1.4.3");
                            google.setOnLoadCallback(function() {
                                jQuery("#im_pay").submit(); 
                            }); 
                        }
                    </script>';
        }

        function process_payment($order_id) {
            $order = new WC_Order($order_id);
            if (!version_compare(WOOCOMMERCE_VERSION, '2.1.0', '<'))
                return array(
                    'result' => 'success',
                    'redirect' => $order->get_checkout_payment_url(true)
                );
            return array(
                'result' => 'success',
                'redirect' => add_query_arg('order-pay', $order->id, add_query_arg('key', $order->order_key, get_permalink(woocommerce_get_page_id('pay'))))
            );
        }

        function receipt_page($order) {
            echo '<p>' . __('Сейчас Вы будете перемещены на страницу оплаты', 'woocommerce') . '</p>';
            echo $this->generate_form($order);
        }

        function ob_exit($status = null) {
            if ($status) {
                ob_end_flush();
                exit($status);
            } else {
                ob_end_clean();
                header("HTTP/1.0 200 OK");
                echo "OK";
                exit();
            }
        }

        function check_im() {
            return in_array($_SERVER['REMOTE_ADDR'], array("194.147.107.254", "91.212.151.242", "127.0.0.1"));
        }

        function checkRequest($posted) {
            ob_start();

            list($eshopId, $orderId, $serviceName, $eshopAccount, $recipientAmount, $recipientCurrency, $paymentStatus, $userName, $userEmail, $paymentData, $secretKey, $hash) = array($_REQUEST['eshopId'], $_REQUEST['orderId'], $_REQUEST['serviceName'], $_REQUEST['eshopAccount'], $_REQUEST['recipientAmount'], $_REQUEST['recipientCurrency'], $_REQUEST['paymentStatus'], $_REQUEST['userName'], $_REQUEST['userEmail'], $_REQUEST['paymentData'], $_REQUEST['secretKey'], $_REQUEST['hash']);

            $order = new WC_Order($orderId);

            // Проверка по секретному ключу
            if ($secretKey) {
                //Проверка источника данных (по секретному ключу)
                if ($secretKey != $this->im_secretKey) {
                    $err = "ERROR: SECRET_KEY MISMATCH!\n";
                    $err .= $this->check_im() ? ("secretKey: $secretKey; order_secretKey: " . $this->im_secretKey . ";\n\n") : "\n";
                    $this->ob_exit($err);
                }

                //Проверка номера сайта продавца
                if ($eshopId != $this->im_eshopId) {

                    $err = "ERROR: INCORRECT ESHOP_ID!\n";
                    $err .= "eshopId: $eshopId; order_eshopId: " . $this->im_eshopId . ";\n\n";
                    $this->ob_exit($err);
                }
            }
            // Проверка по контрольной подписи
            else {
                $control_hash_str = implode('::', array(
                    $eshopId, $orderId, $serviceName,
                    $eshopAccount, $recipientAmount, $recipientCurrency, $paymentStatus,
                    $userName, $userEmail, $paymentData, $this->im_secretKey
                ));

                $control_hash = md5($control_hash_str);
                $control_hash_utf8_str = (iconv('windows-1251', 'utf-8', $control_hash_str));
                $control_hash_utf8 = md5($control_hash_utf8_str);

                if (($hash != $control_hash && $hash != $control_hash_utf8) || !$hash) {

                    $err = "ERROR: HASH MISMATCH\n";
                    $err .= "Control hash string: $control_hash_str;\n";
                    $err .= "Control hash win-1251: $control_hash;\nControl hash utf-8: $control_hash_utf8;\nhash: $hash;\n\n";
                    $this->ob_exit($err);
                }
            }

            if ($paymentStatus == '3' || $paymentStatus == '7') {
                if (wc_get_order_status_name($this->im_paymentStatus5) == wc_get_order_status_name($order->get_status()) ||
                        wc_get_order_status_name('cancelled') == wc_get_order_status_name($order->get_status())) {
                    $err = "ERROR: Order status is not changed! Can't change to status 3\n";
                    $this->ob_exit($err);
                } else {
                    $order->update_status($this->im_paymentStatus3);
                    $this->ob_exit();
                }
            }
            if ($paymentStatus == '4') {
                $order->update_status('cancelled', __('Заказ отменен', 'woocommerce'));
                $this->ob_exit();
            }
            if ($paymentStatus == '5' || $paymentStatus == '6') {
                if (wc_get_order_status_name('cancelled') == wc_get_order_status_name($order->get_status())) {
                    $err = "ERROR: Order status is not changed!  Can't change to status 5\n";
                    $this->ob_exit($err);
                } else {
                    $order->update_status($this->im_paymentStatus5);

                    WC()->cart->empty_cart();
                    $this->ob_exit();
                }
            }
            $this->ob_exit('Unknown error');
        }

        function check_ipn_response() {
            global $woocommerce;

            if (isset($_GET['return']) AND $_GET['return'] == 'result_url') {
                @ob_clean();
                $_POST = stripslashes_deep($_POST);
                $this->checkRequest($_POST);
            } else if (isset($_GET['return']) AND $_GET['return'] == 'success_url') {
                $order = new WC_Order($_GET['orderId']);
                WC()->cart->empty_cart();
                wp_redirect($this->get_return_url($order));
            }
        }

    }

    function add_intellectmoney_gateway($methods) {
        $methods[] = 'WC_IntellectMoney';
        return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'add_intellectmoney_gateway');
}
?>